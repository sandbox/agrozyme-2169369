<?php

function patch_table_menu()
{
    $menu = array();
    $menu['admin/config/development/patch_table'] = array(
        'title' => 'Patch Table',
        'type' => MENU_NORMAL_ITEM,
        'page callback' => 'drupal_get_form',
        'page arguments' => array('patch_table_patch_form'),
        'access arguments' => array('administer site configuration'),
    );
    $menu['admin/config/development/patch_table/patch'] = array(
        'title' => 'Patch',
        'type' => MENU_DEFAULT_LOCAL_TASK,
        'page callback' => 'drupal_get_form',
        'page arguments' => array('patch_table_patch_form'),
        'access arguments' => array('administer site configuration'),
    );
    $menu['admin/config/development/patch_table/reverse'] = array(
        'title' => 'Reverse',
        'type' => MENU_LOCAL_TASK,
        'page callback' => 'drupal_get_form',
        'page arguments' => array('patch_table_reverse_form'),
        'access arguments' => array('administer site configuration'),
    );
    $menu['admin/config/development/patch_table/setting'] = array(
        'title' => 'Setting',
        'type' => MENU_LOCAL_TASK,
        'page callback' => 'drupal_get_form',
        'page arguments' => array('patch_table_setting_form'),
        'access arguments' => array('administer site configuration'),
    );

    return $menu;
}

function patch_table_module_info()
{
    $data = array();

    foreach (system_rebuild_module_data() as $index => $item) {
        if (isset($item->info['hidden']) && ($item->info['hidden'])) {
            continue;
        }

        $info = $item->info;
        $data[$index] = array(
            'name' => $info['name'],
            'directory' => dirname($item->filename),
        );
    }

    return $data;
}

function patch_table_directory()
{
    return DRUPAL_ROOT . '/sites/all/patches';
}

function patch_table_scan()
{
    $module_list = patch_table_module_info();
    $enabled = variable_get('patch_table_enabled', array());
    $data = array();

    foreach (glob(patch_table_directory() . '/*.patch') as $file) {
        $info = pathinfo($file);
        $index = $info['filename'];
        $item = array('enabled' => FALSE, 'name' => '', 'message' => array(), 'time' => filemtime($file));
        list($item['module'], $item['description'], $item['issue'], $item['comment']) = explode('-', $index) + array(
            '',
            '',
            '',
            ''
        );
        $module = $item['module'];

        if ('drupal' == $module) {
            $item['name'] = 'Drupal';
            $item['directory'] = '/';
        } else {
            if (isset($module_list[$module])) {
                $item = $module_list[$module] + $item;
            } else {
                continue;
            }
        }

        if (isset($enabled[$index])) {
            $item['enabled'] = TRUE;
            $item['message'] = $enabled[$index];
        }

        $data[$index] = $item;
    }

    ksort($data);
    return $data;
}

function patch_table_options(array $scan, $reverse = FALSE)
{
    $options = array(
        'absolute' => TRUE,
        'external' => TRUE,
        'https' => TRUE,
        'attributes' => array('target' => '_blank')
    );
    $data = array();

    foreach ($scan as $index => $item) {
        if ($reverse && (FALSE == $item['enabled'])) {
            continue;
        }

        $data[$index] = array(
            'module' => l($item['name'], 'https://drupal.org/project/' . $item['module'], $options),
            'description' => $item['description'],
            'issue' => (ctype_digit($item['issue'])) ? l($item['issue'], 'https://drupal.org/node/' . $item['issue'], $options) : $item['issue'],
            'message' => implode('<br />', $item['message']),
        );
    }

    return $data;
}

function patch_table_run(array $table, $reverse = FALSE)
{
    $binary = variable_get('patch_table_patch_path', '/usr/bin/patch');

    if (is_executable($binary)) {
        $binary = escapeshellcmd($binary);
    } else {
        form_set_error('', t('Unable to execute patch binary, check that the binary exists and is executable.'));
        return;
    }

    $flags = variable_get('patch_table_verbose_message', FALSE) ? '--verbose' : '';

    if ($reverse) {
        $flags .= '-R';
    }

    $scan = patch_table_scan();
    $enabled = variable_get('patch_table_enabled', array());

    foreach ($table as $index => $item) {
        if (empty($scan[$index]['name']) || empty($item)) {
            continue;
        }

        $directory = escapeshellarg(DRUPAL_ROOT . '/' . $scan[$index]['directory']);
        $patch = escapeshellarg(patch_table_directory() . '/' . $index . '.patch');
        $command = sprintf('%s %s -N --binary -b -p1 -d %s -i %s', $binary, $flags, $directory, $patch);
        $output = array();
        exec($command, $output, $return);

        if (variable_get('patch_table_show_command', FALSE)) {
            drupal_set_message(t('Command: @item', array('@item' => $command)));
        }

        if ($reverse) {
            drupal_set_message(t('Reverse: @item', array('@item' => $index)));
            unset($enabled[$index]);
        } else {
            drupal_set_message(t('Patch: @item', array('@item' => $index)));
            $enabled[$index] = $output;
        }
    }

    variable_set('patch_table_enabled', $enabled);
}

function patch_table_header()
{
    return array(
        'module' => t('Module'),
        'description' => t('Description'),
        'issue' => t('Issue'),
        'message' => t('Message'),
    );
}

function _patch_table_form($form, $reverse = FALSE)
{
    if ($reverse) {
        $info = array('options' => TRUE, 'empty' => t('No enabled patch file found'), 'submit' => t('Reverse'));
    } else {
        $info = array('options' => FALSE, 'empty' => t('No patch file found'), 'submit' => t('Patch'));
    }

    $scan = patch_table_scan();
    $options = patch_table_options($scan, $info['options']);

    $form['patch_table'] = array(
        '#type' => 'tableselect',
        '#header' => patch_table_header(),
        '#options' => $options,
        '#empty' => $info['empty'],
    );

    if (FALSE == empty($options)) {
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => $info['submit'],
        );
    }

    return $form;
}

function patch_table_patch_form($form, &$form_state)
{
    return _patch_table_form($form);
}

function patch_table_patch_form_submit($form, &$form_state)
{
    patch_table_run($form_state['values']['patch_table']);
}

function patch_table_reverse_form($form, &$form_state)
{
    return _patch_table_form($form, TRUE);
}

function patch_table_reverse_form_submit($form, &$form_state)
{
    patch_table_run($form_state['values']['patch_table'], TRUE);
}

function patch_table_setting_form($form, &$form_state)
{
    $form['patch_table_clear_message'] = array(
        '#type' => 'checkbox',
        '#title' => t('Clear patch message.'),
    );

    $index = 'patch_table_verbose_message';

    $form[$index] = array(
        '#type' => 'checkbox',
        '#title' => t('Use verbose message.'),
        '#default_value' => variable_get($index, FALSE),
    );

    $index = 'patch_table_show_command';

    $form[$index] = array(
        '#type' => 'checkbox',
        '#title' => t('Show patch command.'),
        '#default_value' => variable_get($index, FALSE),
    );

    $index = 'patch_table_patch_path';

    $form[$index] = array(
        '#type' => 'textfield',
        '#title' => t('Patch binary path'),
        '#description' => t('Enter the full path to your systems patch binary.'),
        '#default_value' => variable_get($index, '/usr/bin/patch'),
        '#required' => TRUE,
    );

    return system_settings_form($form);
}

function patch_table_setting_form_validate($form, $form_state)
{
    $values = $form_state['values'];
    $index = 'patch_table_patch_path';

    if ($values['patch_table_clear_message']) {
        variable_set('patch_table_enabled', array());
    }

    if (FALSE == is_executable($values[$index])) {
        form_set_error($index, t('Unable to execute patch binary, check that the binary exists and is executable.'));
    }
}
