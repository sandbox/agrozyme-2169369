
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Using the module


INTRODUCTION
------------

Current Maintainer: agrozyme <agrozyme@gmail.com>

This module provides a developer tool for managing patches to core
and contributed modules.

It provides a web UI to setting the patch execute file path,
and apply patch files.


INSTALLATION
------------

1. download module compress file from drupal.org.

2. decompress the file and put in site/all/modules/patch_table.

3. Enable the module.

4. Configure them at admin/config/development/patch_table/setting.


USING THE MODULE
-------------

Please put the patches in the path:
sites/all/patches

The patch filename use Drupal suggested naming scheme:
<module>-<description>-<issue>-<comment>.patch

<module> : The module machine name, it is required.

<description> : The description of issue which is fixed by the pathc file.

<issue> : The issue number at drupal.org.

<comment> : The serail number in the same issue.

Note: It assume the patch run succeed.
